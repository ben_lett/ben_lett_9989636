﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public int Choice1 = 0;
        public int Choice2 = 0;
        public string from = "";
        public string txt = "";
        public string sub = "";

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            sub = subtext.Text;
            from = Fromtext.Text;
            txt = messageb.Text;
            Choice1 = ToBox.SelectedIndex;
            Choice2 = inportbox.SelectedIndex;

            MessageDialog CheckMessage = new MessageDialog($"From: {from}\nSubject: {sub}\nthis email is:{Inportchoice(Choice2)}\nMessage:\n\n{txt}");
            CheckMessage.Title = "are you sure you want to send this email?";

            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Send") { Id = 0 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Reset") { Id = 2 });

            CheckMessage.DefaultCommandIndex = 0;
            CheckMessage.CancelCommandIndex = 1;

            var result = await CheckMessage.ShowAsync();

            if ((int)result.Id == 0)
            {
                Application.Current.Exit();
            }
            else if ((int)result.Id == 2)
            {
                ToBox.SelectedIndex = -1;
                Fromtext.Text = String.Empty;
                inportbox.SelectedIndex = -1;
                messageb.Text = string.Empty;
                subtext.Text = string.Empty;
            }
        }

            static string Inportchoice(int Choice2)
        {
                var SecondChoice = "";

                switch (Choice2)
                {
                    case 0:
                        SecondChoice = "Casual";
                        break;
                    case 1:
                        SecondChoice = "Inportant";
                        break;
                    case 2:
                        SecondChoice = "Urgent!";
                        break;
                }
                return SecondChoice;
            }
        }
    }

